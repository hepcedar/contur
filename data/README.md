# Data files

## [DB](DB)
SQLite database files

## [Models](Models)
UFO physics models, example generator steering files, additional limit files

## [Plotting](Plotting)
Some possibly useful plotting macros, config files and scripts which can be used with `contur-plot`

## [Rivet](Rivet)
Rivet routines not yet in the release. You can drop your own new or
modified in here and they will be used in preference to the released ones.

## [Theory](Theory)
SM Theory predictions, in reference yoda files

## [TheoryRaw](TheoryRaw)
Archive of raw inputs used to build the files in the Theory directory, and store of alternative predictions.

## share
A ``share`` directory will be built when you make your contur installation. This contains resources such as the analysis lists,
which are auto-generated from the database.
