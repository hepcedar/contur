# Z-prime B anomaly models

These are the models and settings used in the results discussed here:
https://hepcedar.gitlab.io/contur-webpage/results/ZPBAnom/index.html

favoured.py is a function to plot the favoured region boundaries in the fits in
https://arxiv.org/abs/2110.13518

Width_cut.py similarly plots the region where the width of the Z prime gets too large for the
calculation to be reliable (B3-L2 model, Figs 9 & 10 of https://arxiv.org/abs/2110.13518)

From https://arxiv.org/abs/1904.10954
Zprime_MDM_UFO
Zprime_MUM_UFO
Zprime_TFHM_Mix_UFO

From https://arxiv.org/abs/2110.13518)
Y3_UFO	
DY3_UFO
DY3P_UFO
Zprime_B3L2_UFO
