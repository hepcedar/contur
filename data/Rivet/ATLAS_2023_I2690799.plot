BEGIN PLOT /ATLAS_2023_I2690799/d(?:07|09|1[1,3,5,7,9]|2[1,3,5])
Title=VBS-Enhanced region $\xi <$ 0.4
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d(?:08|1[0,2,4,6,8]|2[0,2,4,6])
Title=VBS-Suppressed region $\xi >$ 0.4
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d0[7,8]
XLabel=$m_{jj}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m_{jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d(?:09|10)
XLabel=$m_{4l}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m_{4l}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d1[1,2]
XLabel=$p_{\mathrm{T},4l}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T},4l}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d1[3,4]
XLabel=$\Delta \phi_{jj}$ [rad]
YLabel=$\mathrm{d}\sigma / \mathrm{d} Delta \phi_{jj}$ [fb/rad]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d1[5,6]
XLabel=$|\Delta y_{jj}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |\Delta y_{jj}|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d1[7,8]
XLabel=$\cos(\theta^*_{12})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \cos(\theta^*_{12})$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d(?:19|20)
XLabel=$\cos(\theta^*_{34})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \cos(\theta^*_{34})$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d2[1,2]
XLabel=$p_{\mathrm{T},jj}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T},jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d2[3,4]
XLabel=$p_{\mathrm{T},4ljj}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T},4ljj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2023_I2690799/d2[5,6]
XLabel=$S_{\mathrm{T},4ljj}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} S_{\mathrm{T},4ljj}$ [fb/GeV]
END PLOT
