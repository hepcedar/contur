# BEGIN PLOT /ATLAS_2024_I2768921*/d44
Title= Transverse momentum $p_\mathrm{T}(\gamma)$ in single-lepton channel
XLabel=$p_\mathrm{T}(\gamma)$ [GeV]
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(\gamma)}$ [fb / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d45
Title= Transverse momentum $p_\mathrm{T}(\gamma)$ in single-lepton channel
XLabel=$p_\mathrm{T}(\gamma)$ [GeV]
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(\gamma)}$ [1 / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d46
Title= Absolute pseudorapidity $|\eta(\gamma)|$ in single-lepton channel
XLabel=$|\eta(\gamma)|$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}|\eta(\gamma)|}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=300
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d47
Title= Absolute pseudorapidity $|\eta(\gamma)|$ in single-lepton channel
XLabel=$|\eta(\gamma)|$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}|\eta(\gamma)|}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=3
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d48
Title= Transverse momentum $p_\mathrm{T}(j_1)$ in single-lepton channel
XLabel=$p_\mathrm{T}(j_1)$ [GeV]
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(j_1)}$ [fb / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=3
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d49
Title= Transverse momentum $p_\mathrm{T}(j_1)$ in single-lepton channel
XLabel=$p_\mathrm{T}(j_1) $ [GeV]
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(j_1)}$ [1 / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d06
Title= Angular separation $\Delta R(\gamma,b)_\mathrm{min}$ in single-lepton channel
XLabel=$\Delta R(\gamma,b)_\mathrm{min}$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,b)_\mathrm{min}}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=200
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d07
Title= Angular separation $\Delta R(\gamma,b)_\mathrm{min}$ in single-lepton channel
XLabel=$\Delta R(\gamma,b)_\mathrm{min}$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,b)_\mathrm{min}}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d08
Title= Angular separation $\Delta R(\gamma,\ell)$ in single-lepton channel
XLabel=$\Delta R(\gamma,\ell)$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,\ell)}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=200
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d09
Title= Angular separation $\Delta R(\gamma,l)$ in single-lepton channel
XLabel=$\Delta R(\gamma,l)$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,l)}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d10
Title= Angular separation $\Delta R(\ell,j)_\mathrm{min}$ in single-lepton channel
XLabel=$\Delta R(\ell,j)_\mathrm{min}$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\ell,j)_\mathrm{min}}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=300
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d11
Title= Angular separation $\Delta R(l,j)_\mathrm{min}$ in single-lepton channel
XLabel=$\Delta R(l,j)_\mathrm{min}$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(l,j)_\mathrm{min}}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d50
Title= Transverse momentum $p_\mathrm{T}(\gamma)$ in dilepton channel
XLabel=$p_\mathrm{T}(\gamma)$ [GeV]
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(\gamma)}$ [fb / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d51
Title= Transverse momentum $p_\mathrm{T}(\gamma)$ in dilepton channel
XLabel=$p_\mathrm{T}(\gamma)$ [GeV]
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(\gamma)}$ [1 / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d52
Title= Absolute pseudorapidity $|\eta(\gamma)|$ in dilepton channel
XLabel=$|\eta(\gamma)|$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}|\eta(\gamma)|}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=50
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d53
Title= Absolute pseudorapidity $|\eta(\gamma)|$ in dilepton channel
XLabel=$|\eta(\gamma)|$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}|\eta(\gamma)|}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d54
Title= Transverse momentum $p_\mathrm{T}(j_1)$ in dilepton channel
XLabel=$p_\mathrm{T}(j_1)$ [GeV]
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(j_1)}$ [fb / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=0.4
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d55
Title= Transverse momentum $p_\mathrm{T}(j_1)$ in dilepton channel
XLabel=$p_\mathrm{T}(j_1)$ [GeV]
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(j_1)}$ [1 / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d12
Title= Angular separation $\Delta R(\gamma,b)_\mathrm{min}$ in dilepton channel
XLabel=$\Delta R(\gamma,b)_\mathrm{min}$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,b)_\mathrm{min}}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=40
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d13
Title= Angular separation $\Delta R(\gamma,b)_\mathrm{min}$ in dilepton channel
XLabel=$\Delta R(\gamma,b)_\mathrm{min}$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,b)_\mathrm{min}}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d14
Title= Angular separation $\Delta R(\gamma,l)_\mathrm{min}$ in dilepton channel
XLabel=$\Delta R(\gamma,l)_\mathrm{min}$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,l)_\mathrm{min}}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
YMax=40
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d15
Title= Angular separation $\Delta R(\gamma,\ell)_\mathrm{min}$ in dilepton channel
XLabel=$\Delta R(\gamma,\ell)_\mathrm{min}$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\gamma,\ell)_\mathrm{min}}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d16
Title= Angular separation $\Delta R(\ell,j)_\mathrm{min}$ in dilepton channel
XLabel=$\Delta R(\ell,j)_\mathrm{min}$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\ell,j)_\mathrm{min}}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
YMax=60
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d17
Title= Angular separation $\Delta R(\ell,j)_\mathrm{min}$ in dilepton channel
XLabel=$\Delta R(\ell,j)_\mathrm{min}$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(\ell,j)_\mathrm{min}}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=0
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/18
Title= Transverse momentum $p_\mathrm{T}(\gamma)$ for combined channels
XLabel=$p_\mathrm{T}(\gamma)$ [GeV]
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(\gamma)}$ [fb / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/19
Title= Transverse momentum $p_\mathrm{T}(\gamma)$ for combined channels
XLabel=$p_\mathrm{T}(\gamma)$ [GeV]
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}p_\mathrm{T}(\gamma)}$ [1 / GeV]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d20
Title= Absolute pseudorapidity $|\eta(\gamma)|$ for combined channels
XLabel=$|\eta(\gamma)|$
YLabel= $\frac{\mathrm{d}\sigma}{\mathrm{d}|\eta(\gamma)|}$ [fb]
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT


# BEGIN PLOT /ATLAS_2024_I2768921*/d21
Title= Absolute pseudorapidity $|\eta(\gamma)|$ for combined channels
XLabel=$|\eta(\gamma)|$
YLabel= $\frac{1}{\sigma} \ \frac{\mathrm{d}\sigma}{\mathrm{d}|\eta(\gamma)|}$
RatioPlotYMin=0.55
RatioPlotYMax=1.45
Legend=1
LogY=1
# END PLOT

