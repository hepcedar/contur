# TO DO

## bin/contur
From the `contur` executable, currently the following strucure is produced:
```
ANALYSIS
    - POOL_1
        - ANA_1A: obs1.dat, obs2.dat, ...
        - ANA_1B: obs1.dat, obs2.dat, ...
        - ...
    - POOL_2
        - ANA_2A, obs1.dat, obs2.dat, ...
        - ...
```
`contur-mkhtml` then triggers rivet `make_plots()` function for every observable 
(`obsX.dat` file) for every analysis (ANA) in the different pools (POOL).

The suggested new layout is the following:
```
ANALYSIS
    - POOL_1
        - ANA_1A: SM.yoda, SMPlusBSM.yoda 
        - ANA_1B: SM.yoda, SMPlusBSM.yoda  
        - ...
    - POOL_2
        - ANA_2A, SM.yoda, dataPlusBSM.yoda,
        - ...
```

Which `.yoda` files are created depends 1) on the analysis, as some do not have a 
SM prediction so we need to use data as bkg and 2) user specifications, who might
toggle the data as bkg flag for the exclusions. 

For a first version, it is probably easiest to put the exclusions in the *Title* of 
each histogram (i.e. observable) of the `SMPlusBSM.yoda` (or `dataPlusBSM.yoda`).
Then it will be automatically be picked up and plotted on the canvas in the next step.

There is no need to write out the reference data as we fetch this in the next step.

## bin/contur-mkhtml-mpl
This goes roughly in three steps:
1. opens the ANALYSIS dirs and subdirs
2. calls `rivet-mkhtml <yodafiles>` for every subdir (i.e. every analysis in 
the selected pools) it can find. This then plots the SM, SM(or Data)+BSM *and*
the reference data.
3. It looks like the main contur html webpage now shows the leading contributions
from each pools, so in the final step we'd need to regroup the analysis this way.

## Comment from Jon

I can see that this is a kind of near-equivalent approach. But I was hoping we could ditch the intermediate file output completely. Something like:

1. contur-mkhtml works just from the results yoda file; if it doesn't find the CLs values it obviously cant' display them, but it can still draw the comparison histograms
2. when you run contur (even on a single yoda) it makes a little results DB like the grid runs do
3. contur-mkhtml will look for a results DB file, and if it finds one will get the CLs from there and make the equivalent output we have now.

What do you think?
