# Running a batch job to Generate Heatmaps

## Setting up the batch job
The command to create multiple YODA files in a batch job is (surprisingly) `contur-batch`. This will send different jobs to the batch system, each job corresponding to a point in your parameter space. Furthermore, 

In the same run-area from [your first run](firstrun.md), run a test scan over the parameter space defined in 'param_file.dat' without submitting it
to a batch farm.  (The `-s` flag ensures no jobs will be submitted.)

         $ contur-batch -n 1000 --seed 101 -s

  or if you have a version of Herwig without the rivet interface installed, use the pipe option:

         $ contur-batch -n 1000 --seed 101 -s -P

These examples initiate a random seed of 101 and generate 1000 events per parameter point. Type `contur-batch --help` to see all the available options.

This will produce a directory called 'myscan00' (or some higher integer if myscan00 already existed) containing one directory for each selection beam energy (just 13TeV pp by default), containing however many runpoint directories are indicated by the ranges in your `param_file.dat`. Have a look at the shell scripts (`runpoint_xxxx.sh`) which have been generated and the `herwig.in` files to check all is as you expected. 

You can manually run one or more `runpoint_xxxx.sh` files to verify your setup before submitting many jobs to the system:

         $ cd myscan00/13TeV/0000
         $ chmod +x runpoint_0000.sh
         $ ./runpoint_0000.sh

In these quick local tests you might want to change the number of events (`--numevents` on the `Herwig run` line of the `runpoint_xxxx.sh` scripts) to something small. Alternatively run Herwig locally using the generated `herwig.in` files, as done previously in the [first run](firstrun.md).

- If you do not encounter any problems running on a single point, you are ready to run batch jobs. Remove the myscan00 directory tree you just created, and run the
  batch submit command again, now without the `-s` flag and specifying the queue
  on your batch farm. For example:

         $ contur-batch -n 1000 --seed 101 -Q medium

  or
  
         $ contur-batch -n 1000 --seed 101 -P -Q medium


  (Note that we assume `qsub` is available on your system here and has a queue called `medium`.
  Slurm and condor batch systems are also supported, and of course you can change the queue name.
  If you have a different submission system you'll need to
  look into `contur/scan/batch_submit.py` and work out how to change the appropriate submission
  commands.)

- A successful run will produce a directory called 'myscan00' as before. You need to wait for the farm
  to finish the jobs before continuing. On PBS, you can check the progress using the 'qstat' command.

- When the batch job is complete there should, in every run point directory, be
  files `herwig-runpoint_xxx.yoda`.

## Analyse results with contur. 

Now it's time to do some statistics with your set of YODA files and set exlucions on the BSM parameter space!

For this, we use the `contur` executable where we point the grid flag `-g` to the scan folder: 

        $ contur -g myscan00/

The output will be a database (.db) file in the ANALYSIS folder.

For various options, see:

        $ contur --help

You may find the `--mpl` flag useful when analysing the runpoints individually. Some more info on this is given [below](#analysing-the-individual-runpoints).

## Plot a heatmap

The `contur_run.db` database file contains all the information to make 2D exclusion plots. The commands are simple:

        $ cd ANALYSIS/
        $ contur-plot --help
        $ contur-plot contur_run.db mY1 mXd  -T "My First Heatmap"

## Analysing the individual runpoints

To look in more detail at an individual parameter point, including looking at the Rivet histograms for each measurement, see [here](singlepoint.md).

## Other functionality

`contur-gridtool` provides some utilities for compressing or merging grids, recovering failed grid points, and locating the files corresponding to a particular parameter point.

