## Running Contur with Madgraph

Instead of Herwig, Contur can also be used in combination with other Monte Carlo generators that provide hepmc files. In addition to this page, you may find the information on [MadGraph tips and tricks](https://gitlab.com/hepcedar/contur/-/wikis/MadGraph-tips-and-tricks) useful.

For Madgraph, it is recommended to use version 2.7.2 or later and the latest Rivet release (>3.1.2). When running

        $ $MG_DIR/bin/mg5_aMC <Madgraph script>

where `$MG_DIR` is the directory containg the Madgraph installation, Madgraph will produce an LHE file containing MC events in e.g. mgevents/Events/Run01. Include `shower=Pythia8` in your Madgraph script to have Pythia shower the events in the LHE file and give a hepmc file as output. This one can be read in with Rivet, giving the yoda file. As Madgraph provides a large number of event weights, it is recommended to use the `--skip-weights` option with Rivet to reduce the number of processed weights with different names.

        $ rivet --skip-weights -a $ANALYSIS-LIST <hepmc file>

For the same reason, when running Contur on the yoda file, use the `--wn "Weight_MERGING=0.000"` option

        $ contur --wn "Weight_MERGING=0.000" <yoda file>

When using `contur-batch`, set the generator name to Madgraph using `-m`:

        $ contur-batch -m madgraph -p <param file> -t <Madgraph script>

Within the param file, make sure the model parameters are set similar to the the way it's done for Herwig, e.g.

        set gPXd        {gPXd}
        set tanbeta     {tanbeta}

By default, Madgraph runs on multiple cores. On a batch system, the jobs are however usually assigned a single core. To configure Madgraph correctly for single core mode and thus use the computational ressources most efficiently, include

        set run_mode 0
        set nb_core 1
        set low_mem_multicore_nlo_generation

in your Madgraph script. Example Madgraph scripts containing the important functionality can be found at data/Models/DM/DM_vector_mediator_UFO/mg-example.sh and data/Models/DM/Pseudoscalar_2HDM/mg-example.sh.

As Madgraph might generate a large quantity of output while running, it is recommended to run the Contur clean-up functionality

        $ contur-gridtool <grid directory>

directly after all jobs have finished.

There is an ongoing project to improve interoperability with Madgraph, see [Open Projects](/hepcedar/contur/-/wikis/Open-and-ongoing-projects).