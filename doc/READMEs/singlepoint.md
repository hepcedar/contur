# Analysing individual runpoints

After generating a yoda file for a single point, or after looking at the 2D exclusion plot and deciding you want to
focus on a single point in the two-dimensional parameter space, you may want to look in more detail at the results.
For example, you might want to understand which specific measurements contribute to the exclusion at that point, and how much. 

This is a straightforward exercise, using a set of plotting scripts that are already generated and stored in `ANALYSIS/plots/`.
Have a browse through this folder and you will find that each histogram for all the analyses have an associated Python script.
By executing these Python scripts you generate plots showing the differential distributions with the data, SM prediction and SM+BSM prediction.

To do this, go back to your run area and show the level of exclusion for every histograms from each analysis as follows (in this example
we are looking at the 13TeV results for the point stored in the 13TeV/0010 directory):

        $ cd ..
        $ contur-rivetplots -i ANALYSIS --runpoint 13TeV/0010 -p
        INFO - Contur version 2.5.0
        INFO - See https://hepcedar.gitlab.io/contur-webpage/
        Writing log to contur.log
        INFO - Read DB file ANALYSIS/contur_run.db
        INFO - Looking for plot scripts in ANALYSIS/plots/13TeV/0010
        ATLAS_13_SSLLMET
        - ATLAS_2019_I1738841
        -- d01-x01-y01 : 0.01(DATABG) 0.01(SMBG) 0.01(EXP) 

        ATLAS_13_METJET
        - ATLAS_2017_I1609448
        -- d02-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        -- d01-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        -- d04-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        -- d03-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        ...
       
You specify the point on the grid with the `--runpoint` command-line argument. The `-p` (`--print`) flag ensures the info per histogram is only printed to the terminal and no plots are generated yet. As you can see in the example above, there might be many histograms that do not contribute (much) to the actual exclusion. It may not be necessary to generate plots for all of these, therefore two options allow you to filter the output by the minimum level of CLs and the name of the analysis, for example:

        $ contur-rivetplots -i ANALYSIS --runpoint 13TeV/0010 -p --cls 0.9 --ana-match ATLAS_13
        INFO - Contur version 2.5.0
        INFO - See https://hepcedar.gitlab.io/contur-webpage/
        Writing log to contur.log
        INFO - Read DB file ANALYSIS/contur_run.db
        INFO - Looking for plot scripts in ANALYSIS/plots/13TeV/0010
        ATLAS_13_TTHAD
        - ATLAS_2022_I2077575
        -- d32-x01-y01 : 0.99(DATABG) 1.00(SMBG) 0.99(EXP) 
        -- d05-x01-y01 : 1.00(DATABG) 1.00(SMBG) 1.00(EXP) 
        -- d50-x01-y01 : 0.94(DATABG) 0.99(SMBG) 0.94(EXP) 
        -- d08-x01-y01 : 0.68(DATABG) 1.00(SMBG) 0.68(EXP) 
        -- d41-x01-y01 : 1.00(DATABG) 1.00(SMBG) 1.00(EXP) 
        -- d14-x01-y01 : 0.72(DATABG) 0.97(SMBG) 0.72(EXP) 
        -- d58-x01-y01 : 0.72(DATABG) 0.99(SMBG) 0.72(EXP) 
        -- d44-x01-y01 : 0.79(DATABG) 0.94(SMBG) 0.79(EXP) 
        ...

This command will then only display the histograms using ATLAS 13TeV data with a minimum exclusion of 0.9 (90%). If you are happy with the filter settings, you can remove the `-p` flag and hit enter. This plots the data, SM prediction and SM+BSM prediction for the histograms that pass your filter. You can browse these using the generated HTML booklet:

        $ open ANALYSIS/plots/13TeV/0010/index.html


