# Contur installation using docker

Docker is a container system that should allow you to run contur on any machine. See [docker.com](https://www.docker.com)
for general information on docker and how to install it on your machine.

Once you have installed the docker desktop on you machine, you should be able to pull the contur container that you want, for example:

```docker pull hepstore/contur-herwig```

You can see what containers are available by searching contur on the [docker hepstore hub](https://hub.docker.com/search?q=hepstore). Typically
they will include all the software infrastructure needed to run Contur (including Rivet, for example) and optionally one of the generators commonly
used (herwig, in the example above).


```docker run -it -v $PWD:/host --rm hepstore/contur-herwig```

The `-it` means interactive mode, `-v /some/host/dir:/some/container/dir` maps a directory on your machine to a directory in the container. `--rm` means the container will automatically clean up when you exit.

When your container starts up, you will be in the contur installation directory itself. It is probably best to make a new directory in the
container to work in, e.g. `mkdir /Work; cd /Work`.

From there, you should be able to follow the rest of the tutorials, for example [First contur run](doc/READMEs/firstrun.md).



