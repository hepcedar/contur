# Introduction

This is the README file for Contur developers. It includes instructions on installing and running
Contur directly from your local repository, on profiling the performance of Contur, and the current release procedure.


## Installation for developers

- Check out code from repository. (Assume you start in $HOME.)

        $ git clone https://gitlab.com/hepcedar/contur.git

- This will get you the default version, which is currently the `main` branch. Go into the contur directory
  that will have been created. (If you want to be able to push changes, you should set yourself up for ssh access to gitlab.)

        $ cd contur/

- For getting started, we recommended you use the latest stable release version given [here](/hepcedar/contur/-/releases). The syntax to do this is:

        $ git checkout <tag name>

- Now in your contur directory, do

        $ source setupContur.sh
        $ make

and optionally

        $ make check      

You should then be ready run on yoda etc as described in [first Contur run](firstrun.md). The contur instance that is run will be the one from your local repository, so any changes you make can be tested immediately. Note that `make check` runs regression tests, so can take some time.
  


## Release steps
Before uploading Contur to PyPI, we have the following to work on:

### Profiling before a release

It is a good idead to profiling Contur and check that your changes have not unintentionally or disproportionately increased the
time consumption. This can be done with the command ``contur-profile``. Optimization should be investigated if Contur
slows down significartly (if needed). For example, to view the performance of grid YODA run, you should:

        $ contur-profile -g myscan00

then you can find a ``Profiling_Contur.prof`` file under your ``CONTUR_USER_DIR`` area. There are two kinds of visualization tools, first one is called **snakeviz**, which can transfer ``.prof`` to an icicle plot, you need to:

        $ pip install snakeviz
        $ cd $CONTUR_USER_DIR
        $ snakeviz Profiling_Contur.prof

Second package is called **gprof2dot**, which can transfer ``.prof`` file to a dot graph:

        $ pip install gprof2dot
        $ cd $CONTUR_USER_DIR
        $ gprof2dot −f pstats Profiling_Contur.prof | dot−Tpng −o Profiling_Contur.png

here ``Profiling_Contur.png`` after ``dot-Tpng -o`` is the name of png file that developers can define themselces.
For MacOS user, you may find an error: ``zsh: command not found: dot`` after finishing above step, use the following way to solve and then rerun the gprof2dot command above:

        $ brew install graphviz

### Check the release

After finishing Contur optimization, use pytest to check if anything work smoothly, if it failed, try to fix anything broken and involve updating the regression ref data. To do this firstly you should go into Contur directory then:

        $ make
        $ make check

### Final steps, and upload

Update the version under ``contur/config/versions.py``

Rebuild wheels (you many need to `pip install wheel` first):

        $ python setup.py bdist_wheel

Before uploading, check whether specified wheel’s long description friendly to PyPI:

        $ twine check dist/[name of wheel to upload]

Commit and push if you pass the above steps. Tag the release contur-x.y.z and upload to PyPI:

        $ python3 -m twine upload --repository pypi dist/[name of wheel to upload]

Notes:
- One wheel can only upload to PyPI once, if you want to reupload, you can change wheel's name or delete uploaded wheels.
- If you do not install twine, use:

        $ pip install twine
        
to install it.
- If Contur slows down and the reason might be ``for-loop``, you can try parallel programing package such as **pathos.multiprocessing** and **multiprocessing**. But make sure not to build too much processing pools, which will destroy your computer service(I did it once, so be careful). Also, make sure generated results should be the same as before.
