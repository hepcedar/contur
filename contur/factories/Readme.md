## Statistical method

For the statistical analysis of the measured data and BSM prediction, we imagine conducting a counting experiment and define the likelihood as

$`L(x|\mu s+b):=\frac{\left(\mu s+b\right)^x}{x!}e^{-\left(\mu s+b\right)}`$.

Hereby, we want to be able to determine whether a signal hypothesis $`s`$ with signal strength $`\mu`$ can be excluded given the data $`x`$ and background $`b`$.
As we do not have single bins but instead complete histograms, we take the product of the likelihoods:

$`L(\vec{x}|\mu\vec{s}+\vec{b})=\prod_i L(x_i|\mu s_i+b_i)`$.

$`\vec{x}`$, $`\vec{s}`$ and $`\vec{b}`$ are the yields in the bins of the data, signal and background distributions, respectively.
We exclude the model hypothesis if we can exclude $`\mu=1`$.

We can make use of the profile likelihood ratio (PLR) as test statistic:

$`t_\mu=-2\ln\frac{L\left(\vec{x}|\vec{s}+\vec{b}\right)}{L\left(\vec{x}|\hat{\mu}\vec{s}+\vec{b}\right)}`$

where $`\hat{\mu}`$ corresponds to the signal strength parameter that maximises the likelihood, $`\hat{\mu}=\arg\max_{\mu}~L(\vec{x}|\mu\vec{s}+\vec{b})`$.
In the numerator, we set $`\mu=1`$.
This corresponds to a hypothesis test where the null hypothesis of having a signal strength of $`\mu=1`$ is compared to the alternative hypothesis of the global likelihood maximum.
We assume large statistics and well behaved, i.e. Gaussian distributed, uncertainties. Consequently, we do not need to marginalise nuisance parameters but instead can treat them in the covariance matrix.

In the large-sample limit, $`t_\mu`$ can be approximated using Wald's theorem (following [[Cowan _et al_. 2010](https://arxiv.org/abs/1007.1727)]) as

$`t_\mu=\frac{\left(\mu-\hat{\mu}\right)^2}{\sigma^2}+\mathcal{O}\left(\frac{1}{\sqrt{N}}\right)\approx\frac{\left(1-\hat{\mu}\right)^2}{\sigma^2}`$.

Hereby, $`\hat\mu`$ is the profiled signal strength and is distributed acording to a normal distributions with standard deviation $`\sigma`$.
In the last step, the assumed $`\mu=1`$ is applied.
The probability density function of this approximate test statistic is a noncentral chi-square function.
Again in the large sample limit, this probability density function can be approximated by a central chi-square distribution ($`\chi_n^2`$):

$`f(t_\mu)\approx\frac{1}{\sqrt{2\pi t_\mu}}e^{-\frac{1}{2}t_\mu}\approx\chi_{n=1}^2`$

The degrees of freedom $`n`$ for this distribution are equal to the difference in degrees of freedom between null and alternative hypothesis.
In our case, the alternative hypothesis is fixed in one parameter less than the null hypothesis, i.e. $`n=1`$.

The p-value $`p_\mu`$ for the signal+background hypothesis is then

$`p_\mu\approx\int_{t_\mu^\textnormal{obs}}^\infty \chi^2_1\approx 1-\Phi(\sqrt{t_\mu})`$,

where $`t_\mu^\textnormal{obs}`$ is the observed value of the test statistic.
In the last step, the probability density function for the test statistic is approximated by the cumulative distribution function of a normal distribution for $`\sqrt{t_\mu}`$ width zero mean and unit width.

To be conservative, the $`CL_s`$ method is applied. 
We obtain

$`
CL_s
:=\frac{p_\mu}{1-p_b}
=\frac{\int_{t_\mu^\textnormal{obs}}^\infty f(t_\mu)\mathrm{d}t_\mu}{\int_{t_0^\textnormal{obs}}^\infty f(t_0)\mathrm{d}t_0}
\approx\frac{\int_{t_\mu^\textnormal{obs}}^\infty \chi^2_1}{\int_{t_0^\textnormal{obs}}^\infty \chi^2_1}
\approx\frac{1-\Phi(\sqrt{t_\mu})}{1-\Phi(\sqrt{t_0})}
`$

where in the denominator we use $`\mu=0`$ to test the background-only hypothesis. If values of $`CL_s>1`$ are obtained, i.e. better agreement between BSM hypothesis and data is found than between SM prediction and data, they will be discarded because at the moment Contur is solely targetted at excluding BSM models.

In a last step, we determine $`t_{\mu'}^\textnormal{obs}`$ as

$`t_\mu^\textnormal{obs}\approx \chi^2_{\mu,\textnormal{obs}}=\left( \vec{x} - \mu\cdot\vec s-\vec b\right)^\text{T}\cdot\text{Cov}^{-1}\cdot\left( \vec{x}-\mu\cdot\vec s-\vec b\right)`$

$`t_0^\textnormal{obs}\approx\chi^2_{0, \textnormal{obs}}=\left( \vec{x}-\vec b\right)^\text{T}\cdot\text{Cov}^{-1}_\textnormal{bkg-only}\cdot\left(\vec{x}-\vec b\right)`$

Hereby, the covariance matrices incorporate the various systematic uncertainties reported by the measurements as well as the statistical uncertainties for data, SM prediction and -- in the case of $`t_\mu^\textnormal{obs}`$ -- signal.

In this way, we can obtain the confidence in excluding the given BSM model point for a single histogram from a chosen measurement if the bin-by-bin correlation is given. If it is not available, all bins are considered individual histograms with unit bin number.
