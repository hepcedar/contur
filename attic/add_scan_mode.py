"""
The slice plotting feature relies on the scan_mode table in the db
This script adds scan_mode to the db from the param_file.dat used to create the grid
"""
import numpy as np
import argparse
import os
from argparse import ArgumentParser
import sqlite3 as db
from configobj import ConfigObj

import contur.config.config as cfg
from contur.scan.os_functions import sanitise_inputs #, read_param_steering_file
from contur.scan.scanning_functions import generate_points
from contur.data.data_access_db import get_model_id

parser = ArgumentParser()
parser.add_argument('db_path')
parser.add_argument('param_file_path')
args = parser.parse_args()
db_path = args.db_path
param_file_path = args.param_file_path

if not os.path.isfile(db_path):
    raise FileNotFoundError('Unable to find db file: {}'.format(db_path))

if not os.path.isfile(param_file_path):
    raise FileNotFoundError('Unable to find param file: {}'.format(param_file_path))

def read_param_steering_file(filename=""):
    """
    Read in the parameter card and convert to a python dict

    :param filename: name of the steering file. If empty, the value from cfg is used

    """

    if filename == "":
        filename = cfg.param_steering_file

    config = ConfigObj(filename)
    config.walk(sanitise_inputs, call_on_sections=True)

    try:
        param_dict = config["Parameters"]
    except:
        #cfg.contur_log.error("Error reading {}".format(filename))
        raise KeyError("Input parameter file must contain Parameters block")

    try:        
        run_dict = config["Run"]
        run_args = run_key_list
        for run_arg in run_args:
            if run_arg in run_dict.keys():
                for path in run_dict[run_arg]:
                    if not os.path.exists(path):
                        print("Warning: For %s setup,  %s does not exist!\n" %
                              (run_arg, path))
                        if not cutil.permission_to_continue("Do you wish to continue?"):
                            sys.exit()
    except KeyError:
       #cfg.contur_log.info("No run block in {}. Will use environment variables".format(filename))
       run_dict = None



    for param_name, param_config_dict in param_dict.items():
        # apart from "block" (which is used with SLHA files), these are actually required, not just allowed.
        allowed_modes = {
            "LOG": {"start", "stop", "number","block"},
            "LIN": {"start", "stop", "number","block"},
            "CONST": {"value","block"},
            "REL": {"form","block"},
            "DIR": {"name"},
            "SINGLE": {"name"},
            "SCALED": {"name"},
            "DATAFRAME": {"name"},
            "CONDITION": {"form"}
        }

        def fmt_iter(iterable):
            if len(iterable) == 0:
                return "(None)"
            return ", ".join(iterable)

        try:
            param_mode = param_config_dict["mode"]
        except KeyError:
            error_message = """
            All parameters in {param_steering_file} must have a mode assigned.
            Choose from either: {options}
            """.format(param_steering_file=filename, options=fmt_iter(allowed_modes.keys()))
            raise ValueError(textwrap.dedent(error_message))
        try:
            required_param_keys = allowed_modes[param_mode]
        except KeyError:
            raise ValueError("Param mode \"{param_mode}\" not allowed. Please, "
                             "choose from {options}".format(
                param_mode=param_mode, options=fmt_iter(allowed_modes.keys())))

        param_keys = set(param_config_dict.keys())

        # We spare from including "mode" in every entry in the dict
        # `allowed_modes`, as it is obvious that each mode must include the
        # mode key. However, when checking its presence, we need to include it.
        complete_req_param_keys = required_param_keys | {"mode"}
        missing_keys=fmt_iter(complete_req_param_keys - param_keys)
        
        if param_keys != complete_req_param_keys and not missing_keys=="block":
            error_message = """
            In parameter file "{param_steering_file}", parameter block "{param_name}", 
            mode "{param_mode}" requires keys: {required_keys}.
                Missing keys: {missing_keys}.
                Disallowed keys: {disallowed_keys}.
            """.format(
                param_steering_file=filename,
                param_name=param_name,
                param_mode=param_mode,
                required_keys=fmt_iter(complete_req_param_keys),
                missing_keys=fmt_iter(complete_req_param_keys - param_keys),
                disallowed_keys=fmt_iter(param_keys - complete_req_param_keys),

            )
            raise ValueError(textwrap.dedent(error_message))

    return param_dict, run_dict


# parse param file
param_dict, run_dict = read_param_steering_file(param_file_path)
# print(f'param dict: {param_dict}')
# add the run points
param_dict, num_points = generate_points(param_dict)
# print(f'new param dict: {param_dict}')

conn = db.connect(db_path)
c = conn.cursor()

# add the new table
c.execute('''create table if not exists scan_mode
                   (id integer primary key AUTOINCREMENT,
                   model_id  integer     not null,
                   map_id integer not null,
                   parameter_id integer,
                   parameter_name varchar(255), 
                   mode varchar(255),
                   foreign key(model_id) references model(id) on delete cascade on update cascade,
                   foreign key(map_id) references map(id) on delete cascade on update cascade,
                   foreign key(parameter_id) references parameter(id) on delete cascade on update cascade);''')

# scope model and mad ids from the first row
model_id = c.execute("select model_id from model_point;").fetchone()[0]
map_id = c.execute("select id from map order by id desc;").fetchone()[0]


# populate scan_mode table
for param, values in param_dict.items():
    res = c.execute("select id from parameter where model_id = ? and name = ?;",
                                    (str(model_id), param,)).fetchone()


    # can't find parameter id so insert as null
    if res is None:
        c.execute("insert into scan_mode (id, model_id, map_id, parameter_name, mode)\
                                values (?,?,?,?,?);", (None, model_id, map_id, param, values['mode']))
    else:
        parameter_id = res[0]
        c.execute("insert into scan_mode (id, model_id, map_id, parameter_id, parameter_name, mode) \
            values (?,?,?,?,?,?)", (None, model_id, map_id, param, values['mode']))

conn.commit()
conn.close()