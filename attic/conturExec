#!/usr/bin/env python

import contur as ct
# this is just used to check if a histo is a valid contur histo, this should be
# factored into histFact and taken out
from contur import data as tf
from contur.scan import os_functions as osf
from optparse import OptionParser
from copy import copy
import contur.util as util
from contur.scan.os_functions import read_param_dat
import sys
import os
import cPickle as pickle
import rivet
import logging
import subprocess
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


parser = ArgumentParser(
    usage=__doc__, formatter_class=ArgumentDefaultsHelpFormatter)

pos = parser.add_argument_group("Input/Output control options")

dress = parser.add_argument_group("Dressing options to embelish outputs")

parser.add_argument('yoda_files', nargs='*', metavar='yoda_files',
                    help='.yoda files to process.')

pos.add_argument("-o", "--outputdir", dest="OUTPUTDIR",
                 default="plots", help="Specify output directory for output.")
pos.add_argument("-a", "--analysisdir", dest="ANALYSISDIR",
                 default="ANALYSIS", help="Output directory for analysis "
                 "cards.")
dress.add_argument("-n", "--nostack",
                   action="store_true", dest="NOSTACK", default=False,
                   help="in single run mode, do not stack the histograms in dat "
                   "file output")
parser.add_argument("-y", "--yodamerge",
                    action="store_true", dest="MERGEONLY", default=False,
                    help="only do the yodamerge step, then exit ")
# parser.add_argument("-f","--fastload",
#                   action="store_true", dest="FASTLOAD", default=False,
#                   help="speed up loading of REF data. Only works when running on a single yoda file.")
parser.add_argument("-v", "--version", action="store_true", dest="printVersion",
                    default=False, help="print version number and exit.")
pos.add_argument("-g", "--grid", dest="GRID", default=None,
                 help="specify a folder containing a structured grid of points"
                       " to analyse. Usually 'myscan'.")
pos.add_argument('-T', '--tag', dest='TAG', default='runpoint_',
                 help='Identifier for yoda files when using yoda_merger.')
dress.add_argument("-p", "--param_file", dest="PARAM_FILE", default="params.dat",
                   help="Optionally specify a parameter file. Only used for documentation, and ignored for grid running.")
dress.add_argument("-m", "--model", dest="MODEL",
                   help="Optionally give name for model used. Only used for documentation.")
dress.add_argument("-bf", "--branchingfraction", dest="BF",
                   help="Optionally give a comma-separated list of branching fractions for which cross sections will be stored in the map file")
dress.add_argument("-me", "--matrixelement", dest="ME",
                   help="Optionally give a comma-separated list of matrix elements for which cross sections will be stored in the map file")
dress.add_argument("-w", "--width", dest="WIDTH",
                   help="Optionally give a comma-separated list of particle widths for which values will be stored in the map file")


parser.add_argument("-d", "--debug", action="store_true", dest="DEBUG",
                    default=False, help="Switch on Debug to all, written to contur.log")
testargs = parser.add_argument_group(
    'Options to Manipulate the constructed test statistic')

testargs.add_argument("-xr", "--nometratio",
                      action="store_true", dest="EXCLUDEMETRAT", default=False,
                      help="Exclude plots where exclusion would be based on a ratio of met/dielptons"
                      "Use this when you have ehnance Z production in your model.")
testargs.add_argument("-xhg", "--nohiggsgamma",
                      action="store_true", dest="EXCLUDEHGG", default=False,
                      help="Exclude plots where Higgs to photons signal is background-subtracted by fitting continuum."
                      "Do this when you have large non-Higgs diphoton production from your model.")
testargs.add_argument("-whw", "--withhiggsww",
                      action="store_true", dest="USEHWW", default=False,
                      help="Include plots where Higgs to WW signal is background-subtracted using data."
                      "Only try this when you have large Higgs WW from your model and not much top or other source of WW.")
testargs.add_argument("-e", "--minsyst", dest="MINSYST", default=0.01,
                      help="Correlated systematic errors with a maximum fractional contribution below this will be ignored (saves time!)")

testargs.add_argument("-c", "--correlations", action="store_true", dest="CORR",
                      default=False, help="Turn on all treatment of correlations")
testargs.add_argument("-th", "--theory", action="store_true", dest="THY", default=False,
                      help="Use theory background models (n.b. ratios will still be used w. theory)")
testargs.add_argument("-xtc", "--notheorycorr", action="store_true", dest="XTHCORR",
                      default=False, help="Assume theory uncertainties are uncorrelated")
testargs.add_argument("-to", "--theoryonly", action="store_true",
                      dest="THONLY", default=False, help="Only use data where theory exists")
testargs.add_argument("-ms", "--minimizerstrategy", dest="MINIMIZERSTRATEGY", default="auto",
                      help="Strategy to get nuisance param best-fits when using correlations: auto, minimizer, or analytic")
testargs.add_argument("--ana-match", action="append", dest="ANAPATTERNS", default=[],
                      help="only write out histograms from analyses whose name matches any of these regexes")
testargs.add_argument("--ana-unmatch", action="append", dest="ANAUNPATTERNS", default=[],
                      help="exclude histograms from analyses whose name matches any of these regexes")


def write_output(message, conturDepot):
    """Temporary function to mimic output of 1 single analysis"""
    util.mkoutdir(opts.ANALYSISDIR)
    sumfn = open(opts.ANALYSISDIR + "/Summary.txt", 'w')

    # summary function will just read the first entry in the depot inbox
    if conturDepot.conturDepotInbox[0].yodaFactory._conturPoint.CLs is not None:
        result = "Combined exclusion for these plots is %.2f %% \n" % (
            conturDepot.conturDepotInbox[0].yodaFactory._conturPoint.CLs * 100.0)
    else:
        result = "Could not evaluate exclusion for these data. Try turning off theory correlations?"

    sumfn.write(message + "\n" + result + "\n")
    sumfn.write("pools")
    for x in conturDepot.conturDepotInbox[0].yodaFactory._sortedBuckets:
        sumfn.write("\n" + x.pools)
        sumfn.write("\n" + str(x.CLs))
        sumfn.write("\n" + rivet.stripOptions(x.tags))
    print result
    sumfn.close()
    # for x in conturDepot.sortedBuckets:
    #     #  does anyone care about these dat files anymore, especially if we
    #     #  have the pickle dump?
    #     util.writeOutput(x.__repr__(), x.pools + ".dat")
    #     with open(opts.ANALYSISDIR + "/" + x.pools + '.map', 'w') as f:
    #         pickle.dump(x, f)
    if opts.GRID:
        conturDepot.writeMap(opts.ANALYSISDIR)
    # with open(opts.ANALYSISDIR + "/combined.map", 'w') as f:
    #     pickle.dump(conturFactory.conturPoint, f)


def write_output_grid(gridPoints):
    util.mkoutdir(opts.ANALYSISDIR)
    path_out = os.path.join(opts.ANALYSISDIR, opts.GRID.strip(os.sep) + '.map')

    with open(path_out, 'w') as f:
        pickle.dump(gridPoints, f)
    print("Writing output map for " + opts.GRID.rstrip(os.sep) + " to : " +
          path_out)


def yoda_merger(scan_dir, tag='runpoint_'):
    """Move through directories merging yoda analysis files"""
    print("Merging yoda files\n"
          "-------------------")
    for root, dirs, files in os.walk(scan_dir):
        file_list = []
        run_point_num = os.path.basename(root)
        out_file = os.path.join(root, 'runpoint_' + run_point_num + '.yoda')
        out_file_gz = out_file+'.gz'

        # remove the LHC.run file to save space.
        run_file = os.path.join(root, 'LHC.run')
        if os.path.exists(run_file):
            command = ' '.join(['rm ', run_file])
            print(command)
            os.system(command)

        if not os.path.exists(out_file_gz):
            if os.path.exists(out_file):
                # runpoint file was there but uncompressed. Compress it.
                command = ' '.join(['gzip ', out_file])
                print(command)
                os.system(command)
            else:
                # runpoint file was not there. Make it.
                for name in files:
                    conditions = (name.endswith('.yoda') and
                                  name != 'LHC.yoda' and
                                  'LHC' in name and
                                  tag in name)
                    if conditions:
                        yodafile = os.path.join(root, name)
                        file_list.append(yodafile)

                if file_list:
                    file_string = ' '.join(file_list)
                    if len(file_list) > 1:
                        command = ' '.join(
                            ['yodamerge -o', out_file, file_string])
                    else:
                        command = ' '.join(['mv', file_string, out_file])
                    print(command)
                    os.system(command)
                    command = ' '.join(['gzip ', out_file])
                    print(command)
                    os.system(command)

                else:
                    logging.warning(
                        'WARNING: NO YODA FILES FOUND IN DIRECTORY ' + run_point_num)
        else:
            logging.warning('runpoint_' + run_point_num +
                            '.yoda.gz already exists.')


#import multiprocessing
#import pathos.pools as pp
#import tqdm
def analyse_grid(scan_path, conturDepot):
    # grid_points = []
    yoda_counter = 0
    yodapaths = []
    parampaths = []

    # pool=pp.ProcessPool(multiprocessing.cpu_count())
    for root, dirs, files in os.walk(scan_path):
        # pool.map(functools.partial(addYodaFile,root=root,dirs=dirs,files=files,yoda_counter=yoda_counter),files)
        for file_name in files:
            valid_yoda_file = (file_name.endswith('.yoda')
                               or file_name.endswith('.yoda.gz')
                               and
                               'LHC' not in file_name and
                               'run' in file_name)
            if valid_yoda_file is True:
                yoda_counter += 1
                yoda_file_path = os.path.join(root, file_name)
                param_file_path = os.path.join(root, 'params.dat')
                yodapaths.append(yoda_file_path)
                parampaths.append(param_file_path)
                params = read_param_dat(param_file_path)
                print '\nFound valid yoda file ' + yoda_file_path.strip('./')
                print 'Sampled at:'
                for param, val in params.iteritems():
                    print param + ': ' + str(val)

                # If requested, grab some values from the log file and add them as extra parameters.
                if opts.ME:
                    params.update(read_out_file(
                        root, files, opts.ME.split(",")))
                if opts.BF or opts.WIDTH:
                    params.update(read_log_file(
                        root, files, opts.BF.split(","), opts.WIDTH.split(",")))

                    # Perform analysis
                conturDepot.addParamPoint(
                    param_dict=params, yodafile=yoda_file_path)
                #contur.addParamPoint(param_dict=params, yodafile=yoda_file_path)
    #pool.map(conturDepot.addParamPointHelper, zip(yodapaths,parampaths))

    # pool.close()
    # pool.join()
    #multiprocessing.Process(target=contur.addParamPoint, args=zip(yodapaths,parampaths))

    print "Found %i yoda files" % yoda_counter
    conturDepot.buildMapAxis()
    # contur.dressParamPoint()
    conturDepot.writeMap(os.path.join(scan_path, opts.ANALYSISDIR))

    # write_output_grid(grid_points)


def read_out_file(root, files, matrix_elements):
    """ Find the Herwig .out file and read various values from it"""
    import re
    me_dict = {}

#    for test_name in files:
#        if test_name.endswith('.out'):
#            out_file_path = os.path.join(root, test_name)
#            with open(out_file_path, 'r') as out_file:
#                for line in out_file.readlines():
#                    for me in matrix_elements:
#                        if not me in me_dict and me in line:
#                            values = line.split()
#                            # dicking around to remove the error surrounded by brackets
#                            num = values[3].split('(')[0]
#                            exp = values[3].split(')')[1]
#                            me_dict[me] = float(num+exp)
#                            print me_dict[me]
#                        break

    for test_name in files:
        if test_name.endswith('.out'):
            out_file_path = os.path.join(root, test_name)
            # with open(out_file_path, 'r') as out_file:
            for me in matrix_elements:
                print me
                with open(out_file_path, 'r') as out_file:
                    mename, mepatt = me, me
                    if "=" in me:
                        mename, mepatt = me.split("=")
                    thisxsec = 0.0
                    for line in out_file.readlines():
                        values = line.split()
                        print values
                        if values and (values[0] == mepatt or re.match(mepatt, values[0])):
                            print line
                            if values[3] != '0':
                                # dicking around to remove the error surrounded by brackets
                                num = values[3].split('(')[0]
                                exp = values[3].split(')')[1]
                                thisxsec = float(num+exp)
                                # print thisxsec
                            else:
                                thisxsec = 0.0
                            me_dict.setdefault(mename, 0.0)
                            me_dict[mename] += thisxsec
                    print me_dict.get(mename)
    return me_dict


def read_log_file(root, files, branching_fractions, widths):
    """ Find the Herwig .log file and read various values from it"""
    bf_dict = {}
    w_dict = {}

    for test_name in files:
        if test_name.endswith('.log'):
            log_file_path = os.path.join(root, test_name)
            with open(log_file_path, 'r') as log_file:
                for line in log_file.readlines():
                    for bf in branching_fractions:
                        if not bf == " " and not bf in bf_dict and bf in line:
                            values = line.split()
                            print values
                            bf_dict[bf] = values[2]
                            print bf_dict[bf]
                        break
                    for w in widths:
                        if not w == " " and not w in w_dict and w in line:
                            values = line.split()
                            print values
                            w_dict[w] = values[9]
                            print w_dict[w]
                        break

    bf_dict.update(w_dict)
    return bf_dict


def read_param_file(file_path):
    """Read a params.dat file and return dictionary of contents"""
    with open(file_path, 'r') as param_file:
        raw_params = param_file.read().strip().split('\n')

    param_dict = {}
    for param in raw_params:
        name, value = param.split(' ')
        param_dict[name] = float(value)

    return param_dict


def main():
    modeMessage = "Run Information \n"

    modeMessage += "Contur is running in " + os.getcwd()
    if opts.GRID:
        modeMessage += " on files in " + opts.GRID + "\n"
    else:
        modeMessage += " on analysis objects in " + str(opts.yoda_files) + "\n"

    # if opts.FASTLOAD:
    #     ct.fastMode=True

    if opts.EXCLUDEHGG:
        ct.excludeHgg = True
        modeMessage += "Excluding Higgs to photons measurements \n"

    if opts.USEHWW:
        ct.excludeHWW = False
        modeMessage += "Including Higgs to WW measurements if available \n"
    else:
        ct.excludeHWW = True
        modeMessage += "Excluding Higgs to WW measurements \n"

    if opts.EXCLUDEMETRAT:
        ct.excludeMETRatio = True
        modeMessage += "Excluding MET ratio measurements \n"

    if opts.printVersion:
        util.writeBanner()
        sys.exit(0)

    if not opts.yoda_files and opts.GRID is None:
        sys.stderr.write("Error: You need to specify some YODA files to be "
                         "analysed!\n")
        sys.exit(1)

    if opts.GRID is not None:
        ct.gridMode = True
    else:
        ct.gridMode = False

    if opts.MINSYST is not None:
        ct.minsyst = opts.MINSYST

    util.writeBanner()
    ct.conturLog.setLevel(logging.WARNING)
    if opts.DEBUG:
        ct.conturLog.setLevel(logging.DEBUG)

        # Make output directory
    if os.path.exists(opts.OUTPUTDIR) and not os.path.realpath(opts.OUTPUTDIR) == os.getcwd():
        import shutil
        shutil.rmtree(opts.OUTPUTDIR)
    try:
        os.makedirs(opts.OUTPUTDIR)
    except:
        print "Error: failed to make new directory '%s'" % opts.OUTPUTDIR
        sys.exit(1)

    # Set the global args used in config.py
    if opts.CORR:
        ct.buildCorr = True
        modeMessage += "Building all available data correlations, combining bins where possible \n"
    else:
        ct.buildCorr = False
        modeMessage += "No correlations being built, using single bins in tests \n"

    if opts.THY or opts.THONLY:
        ct.useTheory = True
        if opts.THONLY:
            ct.theoryOnly = True
            modeMessage += "Using only data where theory calculations for background model are available. \n"
        else:
            modeMessage += "Using theory calculations for background model where availale, data otherwise. \n"
    else:
        ct.useTheory = False
        modeMessage += "Building default background model from data, ignoring optional theory predictions (excluding ratios) \n"

    if opts.XTHCORR:
        ct.useTheoryCorr = False
        if not ct.useTheory:
            ct.conturLog.critical(
                "Option clash, requested uncorrelated theory uncertainty but not using theory")
            sys.exit(1)

        modeMessage += "Theory uncertainties assumed uncorrelated. \n"
    elif ct.useTheory:
        ct.useTheoryCorr = True
        modeMessage += "Theory uncertainty correlations used. \n"

    if opts.MINIMIZERSTRATEGY:
        ct.minimizerStrategy = opts.MINIMIZERSTRATEGY
        modeMessage += "Strategy to find best-fit nuisance param values:%s . \n" % opts.MINIMIZERSTRATEGY

    if opts.ANAPATTERNS:
        ct.onlyAnalyses = opts.ANAPATTERNS
        modeMessage += "Only using analysis objects whose path includes %s. \n" % opts.ANAPATTERNS
    if opts.ANAUNPATTERNS:
        ct.vetoAnalyses = opts.ANAUNPATTERNS
        modeMessage += "Excluding analysis objects whose path includes %s. \n" % opts.ANAUNPATTERNS

    print modeMessage

    contur = ct.conturDepot(gridRun=ct.gridMode,
                            outputDir=opts.OUTPUTDIR, noStack=opts.NOSTACK)

    # rather than porting arguments though class instance initialisations, instead set these as variables in the global config.py
    # all these are imported on import contur so set them here and pick them up when needed later

    if opts.MODEL:
        modeMessage += '\n Model: ' + opts.MODEL
        print '\n Model: ' + opts.MODEL
    if opts.GRID:
        # grid mode
        # --------------------------------------------------------------------------------------------------
        # merge/rename all the yoda files for each beam and parameter point
        yoda_merger(opts.GRID, opts.TAG)

        depots = {}
        # look for beams subdirectories in the chosen grid and analysis each of them
        known_beams = tf.getBeams()
        for beam in known_beams:

            beam_dir = os.path.join(opts.GRID, beam)
            if os.path.exists(beam_dir):
                contur_depot = ct.conturDepot(
                    gridRun=ct.gridMode, outputDir=opts.OUTPUTDIR, noStack=opts.NOSTACK)
                # contur_depot.tagGrid(osf.read_sampled_map(beam_dir))
                analyse_grid(os.path.abspath(beam_dir), contur_depot)
                depots[beam_dir] = contur_depot

        if len(depots) > 0:
            # merge maps for each beam
            print "Merging maps"
            target = None
            for depot in depots.values():
                if not target:
                    target = depot
                    continue
                else:
                    target.mergeMaps(depot)

            if target:
                target.resortPoints()
            target.writeMap(opts.ANALYSISDIR)

        else:
            # look for yoda files in the top level directory and analyse them.
            contur = ct.conturDepot(
                gridRun=ct.gridMode, outputDir=opts.OUTPUTDIR, noStack=opts.NOSTACK)
            # contur.tagGrid(osf.read_sampled_map(opts.GRID))
            analyse_grid(os.path.abspath(opts.GRID), contur)

    else:
        # single mode
        # --------------------------------------------------------------------------------------------------

        contur = ct.conturDepot(gridRun=ct.gridMode,
                                outputDir=opts.OUTPUTDIR, noStack=opts.NOSTACK)
        for infile in opts.yoda_files:

            # get info from paramfile if it is there
            param_file_path = opts.PARAM_FILE
            if os.path.exists(param_file_path):
                params = read_param_dat(param_file_path)
                modeMessage += '\nSampled at:'
                for param, val in params.iteritems():
                    modeMessage += '\n' + param + ': ' + str(val)
            else:
                params = {}
                params["No parameters specified"] = 0.0
                modeMessage += "\nParameter values not known for this run."

            # If requested, grab some values from the log file and add them as extra parameters.
            root = "."
            files = os.listdir(root)
            if opts.ME:
                params.update(read_out_file(root, files, opts.ME.split(",")))
            if opts.BF or opts.WIDTH:
                if not opts.BF:
                    opts.BF = " "
                if not opts.WIDTH:
                    opts.WIDTH = " "
                params.update(read_log_file(
                    root, files, opts.BF.split(","), opts.WIDTH.split(",")))

            # read the yodafile, do the comparison
            contur.addParamPoint(param_dict=params, yodafile=infile)

            # the python logging module is a bit tricky, we'd like to always print this but not have it as an critical!
            # ct.conturLog.info(modeMessage)
            print modeMessage
            # contur.conturDepotInbox[0].yodaFactory)
            write_output(modeMessage, contur)


if __name__ == "__main__":
    opts = parser.parse_args()
    main()
