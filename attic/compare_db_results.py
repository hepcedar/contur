"""
To investigate differences between the results produced by contur's default calculation vs spey
Can either be run in the same directory as the two databases without arguments, or like:
python compare_db_results.py /path/to/contur_run.db /path/to/spey_results.db
N.B: pass the path to the db from the contur calculation first
"""

import os
from argparse import ArgumentParser
import sqlite3 as db
import pandas as pd

parser = ArgumentParser()
parser.add_argument("contur_db",nargs="?",default="contur_run.db", help="Path to the db from the contur calculation")
parser.add_argument("spey_db",nargs="?",default="spey_results.db", help="Path to the db from the spey calculation")



args = parser.parse_args()
spey_db_path = args.spey_db
contur_db_path = args.contur_db

print(f"Reading contur db from {contur_db_path}")
print(f"Reading spey db from {spey_db_path}")

for file_path in [spey_db_path,contur_db_path]:
    if not os.path.exists(file_path):
        raise FileNotFoundError(f"Couldn't find file at {os.path.abspath(file_path)}")


# read dbs into pandas dfs
spey_histo_df = pd.read_sql('select * from obs_exclusions',db.connect(spey_db_path))
ctr_histo_df = pd.read_sql('select * from obs_exclusions',db.connect(contur_db_path))

spey_pool_df = pd.read_sql('select * from exclusions',db.connect(spey_db_path))
ctr_pool_df = pd.read_sql('select * from exclusions',db.connect(contur_db_path))


def join_and_print_analysis(spey_df,ctr_df,analysis_level,print_analysis=True,topn=10):

    # combine exclusions into a single df
    if analysis_level =='histo':
        if spey_df.duplicated(subset=['run_id', 'stat_type', 'histo']).any():
            print("Duplicate rows found in spey_df based on ['run_id', 'stat_type', 'histo']")
        else:
            print("No duplicates found in spey_df")

        # Check for duplicates in contur_df
        if ctr_df.duplicated(subset=['run_id', 'stat_type', 'histo']).any():
            print("Duplicate rows found in contur_df based on ['run_id', 'stat_type', 'histo']")
        else:
            print("No duplicates found in contur_df")

        joined = pd.merge(spey_df,ctr_df, on=['run_id', 'stat_type', 'histo'], how='inner', suffixes=('_spey','_contur'))
    else:
        if spey_df.duplicated(subset=['run_id', 'stat_type', 'pool_name']).any():
            print("Duplicate rows found in spey_df based on ['run_id', 'stat_type', 'pool_name']")
        else:
            print("No duplicates found in spey_df")

        # Check for duplicates in contur_df
        if ctr_df.duplicated(subset=['run_id', 'stat_type', 'pool_name']).any():
            print("Duplicate rows found in contur_df based on ['run_id', 'stat_type', 'pool_name']")
        else:
            print("No duplicates found in contur_df")

        joined = pd.merge(spey_df,ctr_df, on=['run_id', 'pool_name', 'stat_type'], how='inner', suffixes=('_spey','_contur'))

    print(f'joined cols {joined.columns}')
    print(f"len spey: {len(spey_df)} len contur {len(ctr_df)} len joined {len(joined)}")

    joined['difference'] = joined['exclusion_spey'] - joined['exclusion_contur']
    joined['abs_difference'] = abs(joined['difference'])

    if print_analysis:
        print('----Summary-----------------------------------------')
        print(f"Mean exclusion difference: {joined['difference'].mean()}")
        print(f"Number of {analysis_level}s with more than 0.1% difference: {len(joined[joined['abs_difference']>0.001])}")
        print(f"Number of {analysis_level}s with more than 1% difference: {len(joined[joined['abs_difference']>0.01])}")
        print(f"Number of {analysis_level}s with more than 10% difference: {len(joined[joined['abs_difference']>0.1])}")
        print('----Positive and negative differences----------------')
        most_positive_difference_loc = joined['difference'].idxmax()
        most_negative_difference_loc = joined['difference'].idxmin()
        print(f"{analysis_level} where spey most above contur: {joined[analysis_level].loc[most_positive_difference_loc]}, stat type: {joined['stat_type'].loc[most_positive_difference_loc]} spey: {joined['exclusion_spey'].loc[most_positive_difference_loc]} contur: {joined['exclusion_contur'].loc[most_positive_difference_loc]}")
        print(f"{analysis_level} where spey most below contur: {joined[analysis_level].loc[most_negative_difference_loc]}, stat type: {joined['stat_type'].loc[most_negative_difference_loc]} spey: {joined['exclusion_spey'].loc[most_negative_difference_loc]} contur: {joined['exclusion_contur'].loc[most_negative_difference_loc]}")

        # spey above
        print(f"Number of {analysis_level}s where spey more than 0.1% above contur: {len(joined[joined['difference']>0.001])}")
        print(f"Number of {analysis_level}s where spey more than 1% above contur: {len(joined[joined['abs_difference']>0.01])}")
        # spey below
        print(f"Number of {analysis_level}s where spey more than 0.1% below contur: {len(joined[joined['difference']<-0.001])}")
        print(f"Number of {analysis_level}s where spey more than 1% below contur: {len(joined[joined['abs_difference']<-0.01])}")

        stat_types = joined['stat_type'].unique()
        for stat_type in stat_types:
            this_stat_type = joined[joined['stat_type']==stat_type]
            spey_higher = this_stat_type[this_stat_type['difference']>0.0]
            spey_lower = this_stat_type[this_stat_type['difference']<0.0]
            print(f"({stat_type}) When spey above contur, the mean difference is: {spey_higher['difference'].mean()}, spey below contur, the mean difference is: {spey_lower['difference'].mean()}")

        print('----Breakdown by stat type---------------------------')
        for stat_type in stat_types:
            this_stat_type = joined[joined['stat_type']==stat_type]
            num_spey_higher = len(this_stat_type[this_stat_type['difference']>0.0])
            num_spey_lower = len(this_stat_type[this_stat_type['difference']<0.0])
            mean_diff = this_stat_type['difference'].mean()
            print(f"Mean difference for {stat_type}: {mean_diff}, number of {analysis_level}s where spey is higher: {num_spey_higher}, lower: {num_spey_lower} than contur")
        
        print(f'----Top {topn} differences-----------------------------')
        print(f'Top {topn} {analysis_level} where spey exclusion is lower than contur')
        for i, (index, row) in enumerate(joined.nsmallest(topn,'difference').iterrows()):
            print(f"{i+1} spey lower than contur. spey_id: {row['id_spey']}  contur_id: {row['id_contur']} {analysis_level}: {row[analysis_level]}, stat type: {row['stat_type']} spey: {row['exclusion_spey']} contur: {row['exclusion_contur']}")

        
        print(f'Top {topn} {analysis_level} where spey exclusion is higher than contur')
        for i, (index, row) in enumerate(joined.nlargest(topn,'difference').iterrows()):
            print(f"{i+1} spey higher than contur. spey_id: {row['id_spey']}  contur_id: {row['id_contur']} {analysis_level}: {row[analysis_level]}, stat type: {row['stat_type']} spey: {row['exclusion_spey']} contur: {row['exclusion_contur']}")

        # doing this for each histo is too fine grained
        if analysis_level=='pool_name':
            print('----Breakdown by pool and stat type------------------')
            for pool in joined['pool_name'].unique():
                this_pool_only = joined[joined['pool_name']==pool]
                message='Mean difference for '+pool+': '
                for stat_type in stat_types:
                    this_pool_and_stat_only = this_pool_only[this_pool_only['stat_type']==stat_type]
                    if len(this_pool_and_stat_only)==0:
                        mean_diff='(no CLs values)'
                    else:
                        mean_diff = this_pool_and_stat_only['difference'].mean()
                    stat_type_str=stat_type+': '+str(mean_diff)+' '
                    message+=stat_type_str
                print(message)

    return joined

print("************************** POOL LEVEL COMPARISONS ****************************")
pool_joined = join_and_print_analysis(spey_pool_df,ctr_pool_df,analysis_level='pool_name')

print("\n\n\n************************** HISTOGRAM LEVEL COMPARISONS ****************************")
histo_joined = join_and_print_analysis(spey_histo_df,ctr_histo_df,analysis_level='histo')
